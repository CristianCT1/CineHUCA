import React, { Component } from "react";

import { IoLogoGoogle } from "react-icons/io5";
import { IoLogoFacebook } from "react-icons/io5";
import { IoLogoGithub } from "react-icons/io5";
import { Link } from "react-router-dom";
import { signin, signInWithGoogle, signInWithGitHub } from "../helpers/auth";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      email: "",
      password: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.googleSignIn = this.googleSignIn.bind(this);
    this.githubSignIn = this.githubSignIn.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  async handleSubmit(event) {
    event.preventDefault();
    this.setState({ error: "" });
    try {
      await signin(this.state.email, this.state.password);
    } catch (error) {
      this.setState({ error: error.message });
    }
  }

  async googleSignIn() {
    try {
      await signInWithGoogle();
    } catch (error) {
      this.setState({ error: error.message });
    }
  }

  async githubSignIn() {
    try {
      await signInWithGitHub();
    } catch (error) {
      this.setState({ error: error.message });
    }
  }

  render() {
    return (
      <div className="container-login bg-light">
        <div className="card col login-form">
          <div>
            <h2 className="text-center mt-4 fs-1">Login</h2>
          </div>
          <form>
            <div className="mb-3">
              <label htmlFor="inputEmail1" className="form-label">
                Email address
              </label>
              <input
                type="email"
                className="form-control"
                id="inputEmail1"
                aria-describedby="emailHelp"
              />
              <div id="emailHelp" className="form-text">
                We'll never share your email with anyone else.
              </div>
            </div>
            <div className="mb-3">
              <label htmlFor="inputPassword1" className="form-label">
                Password
              </label>
              <input
                type="password"
                className="form-control"
                id="inputPassword1"
              />
            </div>
            <div className="d-grid gap-2 mx-auto">
              <button className="btn btn-dark" type="button">
                Login
              </button>
              <p className="text-center">o</p>
              <button
                onClick={this.googleSignIn}
                type="submit"
                className="btn btn-warning">
                <IoLogoGoogle /> Login with Google
              </button>
              <button
                onClick={this.googleSignIn}
                type="submit"
                className="btn btn-primary">
                <IoLogoFacebook /> Login with Facebook
              </button>
              <button
                onClick={this.googleSignIn}
                type="submit"
                className="btn btn-secondary">
                <IoLogoGithub /> Login with GitHub
              </button>
            </div>
            <div className="mt-3 text-center">
              <p>
                <Link to="/recover-password">Can't login?</Link>
              </p>
              <p>
                <Link to="/register">Sign in to create an account</Link>
              </p>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
