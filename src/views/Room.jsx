import React, { useState } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import FunctionTime from "../components/FunctionTime";
import Seat from "../components/Seat";

function Room({
  cinema = {
    name: "Sala 1",
    seats: [
      { id: 1, price: 15000, available: false, selected: false },
      { id: 2, price: 15000, available: false, selected: false },
      { id: 3, price: 15000, available: true, selected: false },
      { id: 4, price: 15000, available: false, selected: false },
      { id: 5, price: 15000, available: false, selected: false },
      { id: 6, price: 15000, available: true, selected: false },
      { id: 7, price: 15000, available: false, selected: false },
      { id: 8, price: 15000, available: false, selected: false },
      { id: 9, price: 15000, available: false, selected: false },
      { id: 10, price: 15000, available: false, selected: false },
      { id: 11, price: 15000, available: true, selected: false },
      { id: 12, price: 15000, available: true, selected: false },
      { id: 13, price: 15000, available: true, selected: false },
      { id: 14, price: 15000, available: true, selected: false },
      { id: 15, price: 15000, available: true, selected: false },
      { id: 16, price: 15000, available: true, selected: false },
      { id: 17, price: 15000, available: true, selected: false },
      { id: 18, price: 15000, available: true, selected: false },
      { id: 19, price: 15000, available: true, selected: false },
      { id: 20, price: 15000, available: true, selected: false },
      { id: 21, price: 15000, available: true, selected: false },
      { id: 22, price: 15000, available: true, selected: false },
      { id: 23, price: 15000, available: true, selected: false },
      { id: 24, price: 15000, available: true, selected: false },
      { id: 25, price: 15000, available: true, selected: false },
      { id: 26, price: 15000, available: true, selected: false },
      { id: 27, price: 15000, available: true, selected: false },
      { id: 28, price: 15000, available: true, selected: false },
      { id: 29, price: 15000, available: true, selected: false },
      { id: 30, price: 15000, available: true, selected: false },
      { id: 31, price: 15000, available: true, selected: false },
      { id: 32, price: 15000, available: true, selected: false },
      { id: 33, price: 15000, available: true, selected: false },
      { id: 34, price: 15000, available: true, selected: false },
      { id: 35, price: 15000, available: true, selected: false },
      { id: 36, price: 15000, available: true, selected: false },
      { id: 37, price: 15000, available: true, selected: false },
      { id: 38, price: 15000, available: true, selected: false },
      { id: 39, price: 15000, available: true, selected: false },
      { id: 40, price: 15000, available: true, selected: false },
      { id: 41, price: 15000, available: true, selected: false },
      { id: 42, price: 15000, available: true, selected: false },
    ],
    seatingPlan: "OOXOOOOXOOOOXOOOOXOOOOXOOOOXOOOOXOOOOXOOOOOOOOOOOO",
    length: 10,
  },
  shoppingCart,
}) {
  let params = useParams();

  let [seats, setSeats] = useState(cinema.seats);
  let [totalPrice, setTotalPrice] = useState(0);

  let distribution = cinema.seatingPlan
    .match(new RegExp(".{1," + cinema.length + "}", "g"))
    .map((structure) => {
      return structure.match(new RegExp(".{1," + 1 + "}", "g"));
    });

  let index = -1;
  return (
    <div className="container-fluid">
      <div className="row bg-white">
        <div className="col-md-8 pt-5 p-0 my-auto">
          <div className="cinema-screen-container">
            <div className="cinema-screen">
              <svg
                id="screen"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 746 66">
                <path
                  className="screen-color"
                  d="M5.68,63.78,0,36.61A2,2,0,0,1,1.6,34.24C110.33,11.84,238.7,0,373,0S635.67,11.84,744.4,34.24A2,2,0,0,1,746,36.61l-5.64,27.17A2,2,0,0,1,738,65.33c-106.65-22-232.8-33.58-365-33.58S114.69,43.36,8,65.33A2,2,0,0,1,5.68,63.78Z"></path>
              </svg>
            </div>
            <div className="label text-center small">Screen</div>
          </div>
          <div className="room">
            {distribution.map((row, numRow) => {
              return (
                <div
                  className={`seat-row ${numRow}`}
                  style={{
                    gridTemplateColumns: `repeat(${cinema.length}, minmax(0, 1fr))`,
                  }}
                  key={`row-${numRow}`}>
                  {row.map((seat, numCol) => {
                    if (seat === "O") {
                      index++;
                      return (
                        <Seat
                          info={seats[index]}
                          seats={seats}
                          setSeats={setSeats}
                          key={`seat-${index}`}
                        />
                      );
                    }
                    return (
                      <div className="empty" key={`${numRow}-${numCol}`} />
                    );
                  })}
                </div>
              );
            })}
          </div>
        </div>
        <div className="sidebar text-center col-md-4">
          <div className="cart-summary pt-3 contained">
            <h2>Your Summary</h2>
            <FunctionTime />
            <div className="mt-3">
              <div className="cart-summary-row cart-summary-film-info">
                <div className="cart-summary-left">
                  <div className="wrapper-image">
                    <figure className="image">
                      <img
                        src="https://m.guiadelocio.com/var/guiadelocio.com/storage/images/cine/archivo-peliculas/geminis/37423557-6-esl-ES/geminis.jpg"
                        alt="Geminis"
                      />
                    </figure>
                  </div>
                </div>
                <div className="cart-summary-right">
                  <h2>Gemini Man</h2>
                </div>
              </div>
              <div className="cart-items"></div>
              <div className="cart-summary-row cart-summary-row-flex">
                Seat
                <span className="text-right">
                  {shoppingCart.map((item, index) => {
                    if (
                      shoppingCart.length > 1 &&
                      index + 1 !== shoppingCart.length
                    ) {
                      return (
                        <>
                          <span className="d-inline-block">{item.seat.id}</span>
                          <span className="dot-separator">·</span>
                        </>
                      );
                    } else {
                      return (
                        <span className="d-inline-block">{item.seat.id}</span>
                      );
                    }
                  })}
                </span>
              </div>
            </div>
            <div className="cart-summary-row cart-summary-row-flex">
              <span className="h1 text-uppercase">Total:</span>
              <span className="h1 text-right">
                ${" "}
                {shoppingCart
                  .map((item) => item.seat.price)
                  .reduce((count, item) => count + parseFloat(item), 0)}
              </span>
              <button type="button" className="btn btn-dark btn-lg btn-block">
                Purchase
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  shoppingCart: state.shoppingCart.shopingCart,
});

export default connect(mapStateToProps)(Room);
