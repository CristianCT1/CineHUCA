import React from 'react'
import { Link } from 'react-router-dom'

export default function Register() {
  return (
    <div className='container-register bg-light'>
            <div className='card col register-form'>
                <div>
                    <h2 className='text-center mt-4 fs-1'>Creat your account</h2>
                </div>
                <form>
                    <div className='d-flex justify-content-between'>
                        <div>
                            <label htmlFor="inputNameRegister" className="form-label">Name</label>
                            <input type="text" className="form-control" id="inputNameRegister"/>
                        </div>
                        <div>
                            <label htmlFor="inputLastNameRegister" className="form-label">Last Name</label>
                            <input type="text" className="form-control" id="inputLastNameRegister"/>
                        </div>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="exampleInputEmail1" className="form-label">Email address</label>
                        <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
                        <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
                    </div>
                    <div className='d-flex justify-content-between'>
                        <div>
                            <label htmlFor="inputCellPhoneRegister" className="form-label">Cell phone</label>
                            <input type="text" className="form-control" id="inputCellPhoneRegister"/>
                        </div>
                        <div>
                            <label htmlFor="inputDateOfBirthRegister" className="form-label">Date of birth</label>
                            <input type="date" className="form-control" id="inputDateOfBirthRegister"/>
                        </div>
                    </div>
                    <div>
                        <div className="mb-3">
                            <label htmlFor="inputPassword" className="form-label">Password</label>
                            <input type="password" className="form-control" id="exampleInputPassword"/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="inputConfirmPassword" className="form-label">Confirm password</label>
                            <input type="password" className="form-control" id="inputConfirmPassword"/>
                        </div>
                    </div>
                    
                    <div className="d-grid gap-2 mx-auto">
                        <button className="btn btn-dark" type="button">Sign on</button>
                    </div>
                    <div className='mt-3 text-center'>
                        <p><Link to='/login'>Already have an account? Sign in</Link></p>
                    </div>
                </form>
            </div>
        </div>
  )
}
