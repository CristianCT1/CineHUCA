import React, { useEffect } from "react";
import { connect } from "react-redux";
import CardsdContainer from "../components/CardsContainer";
import Header from "../components/Header";
import { setMovies as setMoviesAction } from "../actions/movieActions";

function ListMovies({ loading, error, movies, setMovies }) {
  useEffect(() => {
    setMovies();
  }, [setMovies]);

  return (
    <div>
      <div className="list-movies-cover p-4 p-md-5 mb-4 text-dark rounded bg-dark">
        <Header />
      </div>
      {!loading && movies && <CardsdContainer movies={movies} />}
    </div>
  );
}

const mapStateToProps = (state) => ({
  loading: state.movie.loading,
  error: state.movie.error,
  movies: state.movie.movies,
});

const mapDispatchToProps = {
  setMovies: setMoviesAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(ListMovies);
