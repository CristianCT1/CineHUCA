import React from "react";

import { IoLogoFacebook } from "react-icons/io5";
import { IoLogoYoutube } from "react-icons/io5";
import { IoLogoInstagram } from "react-icons/io5";
import { IoLogoLinkedin } from "react-icons/io5";
import { IoPlayOutline } from "react-icons/io5";
import { Link } from "react-router-dom";

export default function Home() {
  return (
    <div className="home">
      <div className="container-home">
        <div>
          <img
            className=""
            src={require("../resources/img/cinehuca.png")}
            alt=""
          />
          <div className="container-social-networks">
            <a href="http://" target="_blank" rel="noopener noreferrer">
              <IoLogoFacebook size={50} color="black" />
            </a>
            <a href="http://" target="_blank" rel="noopener noreferrer">
              <IoLogoYoutube size={50} color="black" />
            </a>
            <a href="http://" target="_blank" rel="noopener noreferrer">
              <IoLogoInstagram size={50} color="black" />
            </a>
            <a href="http://" target="_blank" rel="noopener noreferrer">
              <IoLogoLinkedin size={50} color="black" />
            </a>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              margin: "20px",
            }}>
            <Link to="/home" type="button" className="btn btn-outline-dark">
              {" "}
              <IoPlayOutline size={25} /> See available movies
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
