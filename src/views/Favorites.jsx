import React from "react";
import { connect } from "react-redux";
import CardsdContainer from "../components/CardsContainer";

function Favorites({ favorites }) {
  return (
    <div>
      <div className="list-movies-cover p-4 p-md-5 mb-4 text-dark rounded bg-dark">
        <div className="col-md-6 px-0">
          <h1 className="display-4 fst-italic">Favorites</h1>
          {favorites.length === 0 && (
            <p className="lead my-3">You don't have favorite movies yet</p>
          )}
        </div>
      </div>
      {favorites && <CardsdContainer movies={favorites} />}
    </div>
  );
}

const mapStateToProps = (state) => ({
  favorites: state.movie.favorites,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);
