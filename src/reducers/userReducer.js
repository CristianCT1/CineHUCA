import * as typeName from "../actions/userActions";

const initialState = {
  user: {},
  authenticated: false
};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case typeName.AUTHENTICATED:
      return { ...state, authenticated: true };
    case typeName.AUTHENTICATED_FAILURE:
      return { ...state, authenticated: false };
    default:
      return state;
  }
}
