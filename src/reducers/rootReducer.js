import { combineReducers } from "redux";
import movieReducer from "./movieReducer";
import shoppingCartReducer from "./shoppingCartReducer";
import userReducer from "./userReducer";

const reducer = combineReducers({
  shoppingCart: shoppingCartReducer,
  movie: movieReducer,
  user: userReducer
});

export default reducer;
