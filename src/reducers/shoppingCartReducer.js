import * as typeName from "../actions/shoppingCartAction";

const initialState = {
  shopingCart: [],
};

export default function shoppingCartReducer(state = initialState, action) {
  switch (action.type) {
    case typeName.ADD_SEAT: {
      let cart = state.shopingCart;
      cart.push({
        seat: action.payload.seat,
      });
      return {
        ...state,
        shopingCart: cart,
      };
    }
    case typeName.REMOVE_SEAT: {
      let cart = state.shopingCart.filter((c) => c.seat.id !== action.payload);
      return {
        ...state,
        shopingCart: cart,
      };
    }
    default:
      return state;
  }
}
