import * as typeName from "../actions/movieActions";

const initialState = {
  loading: false,
  error: false,
  movies: [],
  movie: {},
  favorites: localStorage.getItem("favorites")
    ? JSON.parse(localStorage.getItem("favorites"))
    : [],
};

export default function movieReducer(state = initialState, action) {
  switch (action.type) {
    case typeName.LOADING:
      return { ...state, loading: true };
    case typeName.LOADED_FAILURE:
      return { ...state, loading: false, error: true };
    case typeName.SET_MOVIES:
      return {
        ...state,
        movies: action.payload.movies,
        loading: false,
        error: false,
      };
    case typeName.CHANGE_FAVORITE: {
      let favoritesChange = state.favorites ? state.favorites : [];

      let favorite = favoritesChange.filter((f) => f.id === action.id);
      if (favorite?.length === 0) {
        favoritesChange.push(state.movies.filter((f) => f.id === action.id)[0]);
      } else {
        favoritesChange = favoritesChange.filter((f) => f.id !== action.id);
      }

      localStorage.setItem("favorites", JSON.stringify(favoritesChange));
      return { ...state, favorites: favoritesChange };
    }
    default:
      return state;
  }
}
