import React, { useEffect, useState } from "react";
import { HashRouter, Navigate, Route, Routes } from "react-router-dom";
import { auth } from "./services/firebase";

/* COMPONENTS */

import "./app.css";
import Navbar from "./components/Navbar";
import Home from "./views/Home";
import Main from "./views/Main";
import Login from "./views/Login";
import Register from "./views/Register";
import ListMovies from "./views/ListMovies";
import Room from "./views/Room";
import Favorites from "./views/Favorites";

function App() {
  const [firebaseUser, setFirebaseUser] = useState(
    auth().onAuthStateChanged((user) => (user ? true : false))
  );

  useEffect(() => {
    auth().onAuthStateChanged((user) =>
      user ? setFirebaseUser(true) : setFirebaseUser(false)
    );
  }, []);

  return (
    <HashRouter>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route
          path="/home"
          element={
            firebaseUser ? (
              <>
                <Navbar public={true} />
                <Main />
              </>
            ) : (
              <Navigate to="/login" />
            )
          }
        />
        <Route
          path="/list/:category"
          element={
            firebaseUser ? (
              <>
                <Navbar public={true} />
                <ListMovies />
              </>
            ) : (
              <Navigate to="/login" />
            )
          }
        />
        <Route
          path="/login"
          element={
            <>
              <Navbar public={false} />
              <Login />
            </>
          }
        />
        <Route
          path="/register"
          element={
            <>
              <Navbar public={false} />
              <Register />
            </>
          }
        />
        <Route
          path="/:idMovie/room"
          element={
            firebaseUser ? (
              <>
                <Navbar public={true} />
                <Room />
              </>
            ) : (
              <Navigate to="/login" />
            )
          }
        />
        <Route
          path="/favorites"
          element={
            firebaseUser ? (
              <>
                <Navbar public={false} />
                <Favorites />
              </>
            ) : (
              <Navigate to="/login" />
            )
          }
        />
      </Routes>
    </HashRouter>
  );
}

export default App;
