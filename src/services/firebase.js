// Import the functions you need from the SDKs you need
import firebase from "firebase";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBx9qEtTJPiMu6CACYMDC6NhuYvZH1ZFHc",
  authDomain: "cinehuca.firebaseapp.com",
  projectId: "cinehuca",
  storageBucket: "cinehuca.appspot.com",
  messagingSenderId: "284320862757",
  appId: "1:284320862757:web:76bffe97764b20390f65cc",
  measurementId: "G-E6KR3JVSRE"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export const auth = firebase.auth;
export const db = firebase.database();