import { instance as axios } from "./config";

export const movieServices = () => {
  const getMovies = () => {
    return axios({
      method: "GET",
      url: "/movie/list",
    });
  };
  return { getMovies };
};

export default movieServices();
