import React from "react";
import MovieCard from "./MovieCard";

export default function CardsdContainer({ movies }) {
  return (
    <div className="container m-6 mt-4">
      <div className="row row-cols-5">
        {movies?.map((movie, index) => (
          <MovieCard
            key={index}
            id={movie.id}
            title={movie.title}
            img={movie.img}
          />
        ))}
      </div>
    </div>
  );
}
