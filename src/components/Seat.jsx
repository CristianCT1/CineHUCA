import React from "react";
import { ReactComponent as SeatAvailable } from "../assets/svg/seat_available.svg";
import { ReactComponent as SeatOcuppied } from "../assets/svg/seat_ocuppied.svg";
import Available from "../assets/img/available.png";
import Occupied from "../assets/img/occupied.png";
import Select from "../assets/img/select.png";
import { connect } from "react-redux";
import {
  addSeat as addSeatAction,
  removeSeat as removeSeatAction,
} from "../actions/shoppingCartAction";

function Seat({ info, seats, setSeats, addSeat, removeSeat }) {
  return (
    <div className={`seat ${info.available ? "available" : "occupied"}`}>
      <img
        src={info.available ? (info.selected ? Select : Available) : Occupied}
        alt={"seat"}
        style={{ maxHeight: "80px" }}
        onClick={() => {
          if (info.available && info.selected) {
            // Remover del carrito
            removeSeat(info.id);
            setSeats(
              seats.map((s) => {
                if (s.id === info.id) {
                  s.selected = false;
                }
                return s;
              })
            );
          } else if (info.available) {
            // Agregar al carrito
            addSeat(info);
            setSeats(
              seats.map((s) => {
                if (s.id === info.id) {
                  s.selected = true;
                }
                return s;
              })
            );
          }
        }}
      />
    </div>
  );
}

const mapDispatchToProps = {
  addSeat: addSeatAction,
  removeSeat: removeSeatAction,
};

export default connect(null, mapDispatchToProps)(Seat);
