import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useNavigate } from "react-router-dom";
import { changeFavorite as changeFavoriteAction } from "../actions/movieActions";
function MovieCard({ id, title, img, favorites, changeFavorite }) {
  let navigate = useNavigate();
  const [clickFavorite, setClickFavorite] = useState(false);

  useEffect(() => {}, [clickFavorite]);

  function navigateRoom() {
    navigate(`/${id}/room`);
  }
  return (
    <div className="col mb-4" style={{ maxWidth: 250 }}>
      <div className="card col">
        <button
          className={`btn btn-favorites ${
            favorites.filter((f) => f.id === id).length === 0
              ? "btn-light"
              : "btn-danger"
          }`}
          onClick={() => {
            changeFavorite(id, favorites);
            setClickFavorite(!clickFavorite);
          }}>
          ❤
        </button>
        <img
          src={img}
          className="card-img-top"
          alt={title}
          style={{ cursor: "pointer" }}
          onClick={() => {
            navigateRoom();
          }}
        />
        <div className="card-footer">
          <h5
            className="card-title"
            style={{ cursor: "pointer" }}
            onClick={() => {
              navigateRoom();
            }}>
            {title}
          </h5>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  favorites: state.movie.favorites,
});

const mapDispatchToProps = {
  changeFavorite: changeFavoriteAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(MovieCard);
