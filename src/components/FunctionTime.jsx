import React from "react";

export default function FunctionTime() {
  return (
    <div className="css-1ldn01p">
      <div data-themeui-nested-provider="true" className="css-t42eyi">
        <div id="showtimes" className="css-udl0hl">
          <div className="css-16ilj6a">
            <div>
              <div className="css-17dvixe">
                <div className="css-w7ednw">
                  <div className="css-1vy61x7">
                    <div className="css-1l7mrcz">
                      <div className="css-l5xv05">
                        <div className="keen-slider css-1nrmkzk">
                          <div
                            className="keen-slider__slide"
                            style={{
                              minWidth: "calc(33.3333% - 0px)",
                              maxWidth: "calc(33.3333% - 0px)",
                              transform: "translate3d(0px, 0px, 0px)",
                            }}>
                            <div className="css-68am72">
                              <div className="css-1girimy">
                                <div>
                                  <div className="css-b1gs5x">Tue</div>
                                  <div className="css-17p42j8">31</div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="keen-slider__slide"
                            style={{
                              minWidth: "calc(33.3333% - 0px)",
                              maxWidth: "calc(33.3333% - 0px)",
                              transform: "translate3d(0px, 0px, 0px)",
                            }}>
                            <div className="css-68am72">
                              <div className="css-1girimy">
                                <div>
                                  <div className="css-b1gs5x">Wed</div>
                                  <div className="css-17p42j8">1</div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="keen-slider__slide"
                            style={{
                              minWidth: "calc(33.3333% - 0px)",
                              maxWidth: "calc(33.3333% - 0px)",
                              transform: "translate3d(0px, 0px, 0px)",
                            }}>
                            <div className="css-1kme9l3">
                              <div className="css-1girimy">
                                <div>
                                  <div className="css-b1gs5x">Thu</div>
                                  <div className="css-17p42j8">2</div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="keen-slider__slide"
                            style={{
                              minWidth: "calc(33.3333% - 0px)",
                              maxWidth: "calc(33.3333% - 0px)",
                              transform: "translate3d(0px, 0px, 0px)",
                            }}>
                            <div className="css-1kme9l3">
                              <div className="css-1girimy">
                                <div>
                                  <div className="css-b1gs5x">Fri</div>
                                  <div className="css-17p42j8">3</div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="keen-slider__slide"
                            style={{
                              minWidth: "calc(33.3333% - 0px)",
                              maxWidth: "calc(33.3333% - 0px)",
                              transform: "translate3d(0px, 0px, 0px)",
                            }}>
                            <div className="css-1kme9l3">
                              <div className="css-1girimy">
                                <div>
                                  <div className="css-b1gs5x">Sat</div>
                                  <div className="css-17p42j8">4</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="css-1efmhlg">
                          <div className="css-11uxou5">
                            <svg
                              className="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit"
                              focusable="false"
                              viewBox="0 0 24 24"
                              aria-hidden="true">
                              <path d="M14,7L9,12L14,17V7Z"></path>
                            </svg>
                          </div>
                        </div>
                        <div className="css-q47fe6">
                          <div className="css-11uxou5">
                            <svg
                              className="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit"
                              focusable="false"
                              viewBox="0 0 24 24"
                              aria-hidden="true">
                              <path d="M10,17L15,12L10,7V17Z"></path>
                            </svg>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="css-sithx7">
            <div>
              <div className="clickable css-2egtya">
                <div>
                  <time className="css-1lgx68j">5:15 PM</time>
                </div>
              </div>
            </div>
            <div>
              <div className="clickable css-2egtya">
                <div>
                  <time className="css-1lgx68j">6:30 PM</time>
                </div>
              </div>
            </div>
            <div>
              <div className="clickable css-2egtya">
                <div>
                  <time className="css-1lgx68j">9:30 PM</time>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
