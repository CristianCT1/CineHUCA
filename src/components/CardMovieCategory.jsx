import React from 'react'
import { Link } from 'react-router-dom'

export default function CardMovieCategory({title, description, imageName, categoryURL}) {
  return (
    <div className="col-lg-4">
        <img className="bd-placeholder-img rounded-circle" src={ require(`../resources/img/${imageName}`) } width="140" height="140" href role="img" aria-label="Placeholder: 140x140" preserveAspectRatio="xMidYMid slice" focusable="false" />
        <h2>{ title }</h2>
        <p>{ description }</p>
        <p><Link className="btn btn-secondary" to={ categoryURL }>View movies &raquo;</Link></p>
    </div>
  )
}
