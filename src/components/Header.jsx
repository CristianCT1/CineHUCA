import React from "react";
import { Link } from "react-router-dom";

export default function Header() {
  return (
    <div className="col-md-6 px-0">
      <h1 className="display-4 fst-italic">
        The best movies for your children to enjoy
      </h1>
      <p className="lead my-3">
        The benefits of cinema for children are evident when parents guide their
        children to face problems, draw conclusions for themselves and find
        solutions.
      </p>
      <p className="lead mb-0">
        <Link to="" className="text-dark fw-bold">
          Continue reading...
        </Link>
      </p>
    </div>
  );
}
