import React from "react";

import { VscAccount } from "react-icons/vsc";
import { VscHeart } from "react-icons/vsc";
import { Link } from "react-router-dom";

export default function Navbar(props) {
  return (
    <nav className="navbar navbar-expand-lg bg-light">
      <div className="container-fluid">
        <Link className="navbar-brand" to="/">
          <img
            src={require("../resources/img/cinehuca-logo-name.png")}
            alt=""
            height="30"
            className="d-inline-block align-text-top"
          />
        </Link>
        {
          props.public?
        <div className="options d-flex flex-row-reverse">
          <Link to="/login" style={{ margin: "0 10px 0 10px" }}>
            <VscAccount color="black" size={25} />
          </Link>
          <Link to="/favorites" style={{ margin: "0 10px 0 10px" }}>
            <VscHeart color="black" size={25} />
          </Link>
        </div>: <></> }
      </div>
    </nav>
  );
}
