export const ADD_SEAT = "ADD_SEAT";
export const REMOVE_SEAT = "REMOVE_SEAT";
export const CLEAR_SEAT = "CLEAR_SEAT";

export const addSeat = (seat) => async (dispatch) => {
  dispatch({
    type: ADD_SEAT,
    payload: { seat: seat },
  });
};

export const removeSeat = (id) => async (dispatch) => {
  dispatch({
    type: REMOVE_SEAT,
    payload: id,
  });
};
