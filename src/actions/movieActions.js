import movieServices from "../services/movieServices";

export const LOADING = "LOADING";
export const LOADED_FAILURE = "LOADED_FAILURE";

export const SET_MOVIES = "SET_MOVIES";
export const CHANGE_FAVORITE = "CHANGE_FAVORITE";

export const changeFavorite = (id) => async (dispatch) => {
  dispatch({
    type: CHANGE_FAVORITE,
    id: id,
  });
};

export const loading = () => ({ type: LOADING });

export const loadedFailure = () => ({ type: LOADED_FAILURE });

export const setMoviesAction = (payload) => ({
  type: SET_MOVIES,
  payload,
});

export const setMovies = () => async (dispatch) => {
  dispatch(loading());
  try {
    const data = await movieServices.getMovies().then((response) => {
      return response.data;
    });

    dispatch(
      setMoviesAction({
        movies: data.map((movie) => {
          return {
            id: movie._id,
            title: movie.nombre,
            img: movie.url_imagen,
          };
        }),
      })
    );
  } catch (error) {
    console.log(error);
    dispatch(loadedFailure());
  }
};
