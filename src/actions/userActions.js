export const AUTHENTICATED = "AUTHENTICATED";
export const AUTHENTICATED_FAILURE = "AUTHENTICATED_FAILURE";

export const auth = () => ({ type: AUTHENTICATED });

export const authFailure = () => ({ type: AUTHENTICATED_FAILURE });
